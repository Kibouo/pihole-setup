# Additional setup
- Disable other DHCP server on the network
- Fill in environment vars in a separate `.env` file
  - Change permissions so that only `root` can access this file! (protect credentials)
- Set static IPv4 & IPv6

## Ubuntu specific stuff
### Netplan
Network management is done with Netplan.

- edit `/etc/netplan/50-cloud-init.yaml`
- e.g.:
  ```yaml
  network:
      ethernets:
          eth0:
              dhcp4: false
              dhcp6: false
              addresses: [192.168.x.x/yy, 2a02:a03f:8caf:ad00:xxxx:xxxx:xxxx:xxxx/yy]
              gateway4: 192.168.z.z
              nameservers:
                  addresses: [1.1.1.1,8.8.8.8]
              match:
                  driver: bcmgenet smsc95xx lan78xx
              optional: true
              set-name: eth0
      version: 2
  ```
- `netplan try` to test config
- `netplan apply` to save

Additionally, tweak `systemd-resolved` as described here: https://github.com/pi-hole/docker-pi-hole#installing-on-ubuntu